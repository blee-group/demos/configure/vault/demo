# Vault Demo

Simple Vault integration with GitLab CI. The pipeline will run when triggered against the `main` branch.

## Vault

- `VAULT_ADDR` set in CI/CD Settings at group level
- Token: `jwt`
- Policies:
	- `gitlab-demo-staging` = `["read"]` path `gitlab-demo/staging/*`
	- `gitlab-demo-prod` = `["read"]` path `gitlab-demo/prod/*`

### Roles

This project uses two roles matching the policies:

#### `gitlab-demo-staging`

For `staging` environment on a protected branch.

```shell
vault write auth/jwt/role/gitlab-demo-staging - <<EOF
{
  "role_type": "jwt",
  "policies": ["gitlab-demo-staging"],
  "token_explicit_max_ttl": 60,
  "user_claim": "user_email",
  "bound_claims": {
    "project_id": "40611423",
    "ref_protected": "true",
    "environment": "staging"
  }
}
EOF
```

#### `gitlab-demo-prod`

For `production` environment on a protected branch.

```shell
vault write auth/jwt/role/gitlab-demo-prod - <<EOF
{
  "role_type": "jwt",
  "policies": ["gitlab-demo-prod"],
  "token_explicit_max_ttl": 60,
  "user_claim": "user_email",
  "bound_claims": {
    "project_id": "40611423",
    "ref_protected": "true",
    "environment": "production"
  }
}
EOF
```

### Secrets Engine

- Type: Static - KV-V1
- Path: `gitlab-demo/`

### `key:value` pairs

- `staging/db`
	- fields: `user`,`pass`
- `prod/db`
	- fields: `user`,`pass`

